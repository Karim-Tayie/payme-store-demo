import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class SignupSelectionService {
  typeSelected = new EventEmitter<boolean>();
  personType: boolean;

  setPersonType(pType: boolean) {
    this.personType = pType;
    this.typeSelected.emit(this.personType);
  }

  getPersonType() {
    return this.personType;
  }
}
