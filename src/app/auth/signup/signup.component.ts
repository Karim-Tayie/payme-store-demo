import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { SignupSelectionService } from './signup-selection.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  personType: boolean;

  constructor(private dialog: MatDialog,
              private signupSelectionService: SignupSelectionService) { }

  ngOnInit() {
    this.signupSelectionService.typeSelected.subscribe(
      (pType: boolean) => {
        this.personType = pType;
        // console.log(this.personType);
      }
    );
    this.personType = this.signupSelectionService.getPersonType();
  }

}
