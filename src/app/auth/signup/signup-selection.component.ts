import { Component } from '@angular/core';

@Component({
  selector: 'app-signup-selection',
  template: `<h1 mat-dialog-title>Are you a buyer or a seller?</h1>
             <mat-dialog-actions>
                <button mat-stroked-button [mat-dialog-close]="true" color="primary">Buyer</button>
                <button mat-stroked-button [mat-dialog-close]="false" color="accent">Seller</button>
             </mat-dialog-actions>`
})
export class SignupSelectionComponent {
  constructor() {}
}
