import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup-seller',
  templateUrl: './signup-seller.component.html',
  styleUrls: ['./signup-seller.component.css']
})
export class SignupSellerComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  randomUrl;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      firstNameCtrl: ['', Validators.required],
      lastNameCtrl: ['', Validators.required],
      mobileNumberCtrl: ['', Validators.required],
      emailCtrl: ['', Validators.required],
      businessCtrl: ['', Validators.required],
      passwordCtrl: ['', Validators.required],
      checkBoxCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      productNameCtrl: ['', Validators.required],
      productCategoryCtrl: ['', Validators.required]
    });

    this.randomUrl = this.generateRandomURL();
  }

  onAddProduct() {

  }

  onDeleteProduct() {

  }

  generateRandomURL() {
    const lower = 0;
    const upper = 100000000;
    const url = 'https://www.paymestore.co/products?' + (Math.floor(Math.random() * (upper - lower)) + lower);
    return url;
  }
}
