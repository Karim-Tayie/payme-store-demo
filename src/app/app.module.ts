import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { SignupSelectionComponent } from './auth/signup/signup-selection.component';
import { SignupBuyerComponent } from './auth/signup/signup-buyer/signup-buyer.component';
import { SignupSellerComponent } from './auth/signup/signup-seller/signup-seller.component';
import { SignupSelectionService } from './auth/signup/signup-selection.service';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HeaderComponent,
    SidenavListComponent,
    SignupSelectionComponent,
    SignupBuyerComponent,
    SignupSellerComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [SignupSelectionService],
  bootstrap: [AppComponent],
  entryComponents: [SignupSelectionComponent]
})
export class AppModule { }
