import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';

import { SignupSelectionComponent } from '../../auth/signup/signup-selection.component';
import { SignupSelectionService } from '../../auth/signup/signup-selection.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {
  @Output() closeSidenav = new EventEmitter<void>();

  constructor(private dialog: MatDialog,
              private signupSelectionService: SignupSelectionService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onClose() {
    this.closeSidenav.emit();
  }

  onSelect() {
    const dialogRef = this.dialog.open(SignupSelectionComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.signupSelectionService.setPersonType(result);
      this.router.navigate(['signup'], {relativeTo: this.route});
    });
  }
}
