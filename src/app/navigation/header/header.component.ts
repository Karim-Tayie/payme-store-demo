import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';

import { SignupSelectionComponent } from '../../auth/signup/signup-selection.component';
import { SignupSelectionService } from '../../auth/signup/signup-selection.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() sidenavToggle = new EventEmitter<void>();

  constructor(private dialog: MatDialog,
              private signupSelectionService: SignupSelectionService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  onSelect() {
    const dialogRef = this.dialog.open(SignupSelectionComponent);

    dialogRef.afterClosed().subscribe(result => {
      // console.log(result);
      this.signupSelectionService.setPersonType(result);
      this.router.navigate(['signup'], {relativeTo: this.route});
    });
  }
}
